export const getActionResult = (action, data) => {
  switch (action) {
    case "fetching":
      return {
        isFetching: true,
        data: {},
        error: null
      };
    case "success":
      return {
        isFetching: false,
        data,
        error: null
      };
    case "failed":
      return {
        isFetching: false,
        data: {},
        error: data
      };
    default:
      return this.state.otpResponce;
  }
};

export const isTokenAvailable = () => {
  let atemptedToken = window.localStorage.getItem("access_token");
  return atemptedToken !== null && atemptedToken !== "";
};

export const yearRange = [2018, 2019, 2020];
export const monthRange = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];
