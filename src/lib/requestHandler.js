import configurations from "./_config";
class RequestHandler {
  static assignmentDomain = configurations.assignmentDomain;
  static assignmentPath = configurations.assignmentPath;
  static async request(method, path, payload, config = {}, domain = this.assignmentDomain) {
    let url = this.assignmentDomain + this.assignmentPath + path;

    let headerConfig = {
      "Content-Type": "application/json",
      'Access-Control-Allow-Origin':'*'
    };
    if (config.requiredToken) {
      payload.token = localStorage.getItem("access_token");
    }

    if (method === "GET" || method === "GETFILE") {
      let urlParams = new URLSearchParams(payload).toString();
      url = `${url}?${urlParams}`;

      console.log(url);

      return (
        fetch(url, {
          method: "GET",
          headerConfig
        })
          .then(responce => {
            if (method === "GETFILE") {
              let file = responce.blob();
              return file;
            } else {
              return responce.json();
            }
          })
          //^returns promise
          .then(data => {
            return data;
          })
          .catch(error => {
            console.error(`
          Request Error
          method=${method},
          url=${url}
          `);
            throw Error(error.message);
          })
      );
    }
  }
}

export default RequestHandler;
