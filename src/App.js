import React from "react";
import { BrowserRouter as Router, Route, Redirect, Switch } from "react-router-dom";
import update from "immutability-helper";
//--
import "./App.scss";
//--
import Login from "./components/Login";
import Home from "./components/Home";
//--
import { isTokenAvailable, getActionResult } from "./lib/_utils";

const PrivateRoute = ({ component: Component, requiredProps, ...otherProps }) => {
  let isAuthed = isTokenAvailable();
  return (
    <Route
      {...otherProps}
      render={props => {
        if (isAuthed) {
          return <Component {...props} {...requiredProps} />;
        } else {
          return (
            <Redirect
              to={{
                pathname: "/login"
              }}
            />
          );
        }
      }}
    />
  );
};

class App extends React.Component {
  constructor() {
    super();
    // ^ will be global
    this.state = {
      profile: {
        isFetching: false,
        data: {},
        error: null
      },
      invoice: {
        isFetching: false,
        data: {},
        error: null
      }
    };
  }

  responseHandler = (responceType, action, data = null) => {

    // let newState = {...this.state, [inputType]: eventValue};
    //trying immutibility helper bcoz [[https://twitter.com/dan_abramov/status/988546679115800577]]
    let actionResult = getActionResult(action, data);
    let newState = update(this.state, {
      [responceType]: {
        $merge: {
          ...actionResult
        }
      }
    });
    this.setState(newState);
  };

  render() {
    let requiredProps = {
      responseHandler: this.responseHandler,
      profile: this.state.profile,
      invoice: this.state.invoice
    };
    return (
      <div className="App">
        {!navigator.onLine ? <div className="offline-banner">you are offline</div> : null}
        {/* ^^this does not work consistently across browsers */}
        <Router>
          <Switch>
            <Route path="/(login|)" exact component={Login} />
            <PrivateRoute path="/home" component={Home} requiredProps={requiredProps} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
