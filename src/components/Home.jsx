import React, { Component } from "react";
import { Switch, Route, NavLink } from "react-router-dom";
//--
import Profile from "./Profile";
import Invoice from "./Invoice";
//--
import RequestHandler from "./../lib/requestHandler";

class Home extends Component {
  componentWillMount() {
    let props = this.props;
    let config = {
      requiredToken: true
    };
    props.responseHandler("profile", "fetching");
    RequestHandler.request("GET", "/get-basic-info-of-user", {}, config)
      .then(res => props.responseHandler("profile", "success", res))
      .catch(error => props.responseHandler("profile", "failed", error));
  }
  render() {
    const { path } = this.props.match;
    return (
      <section className="homepage">
        <div className="tabs">
          <Switch>
            <Route path={`${path}/profile`} render={props => <Profile {...props} profile={this.props.profile} />} />
            <Route
              path={`${path}/invoice`}
              render={props => <Invoice {...props} invoice={this.props.invoice} responseHandler={this.props.responseHandler} />}
            />
          </Switch>
        </div>
        <nav className="navigation">
          <NavLink to={`${path}/profile`} className="link" activeClassName="active">
            Profile
          </NavLink>
          <NavLink to={`${path}/invoice`} className="link" activeClassName="active">
            Invoice
          </NavLink>
        </nav>
      </section>
    );
  }
}

export default Home;
