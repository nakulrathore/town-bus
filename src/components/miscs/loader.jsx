import React from "react";

const Loader = props => (
  <div
    className={`
        loader 
        ${props.isLight ? "light" : "dark"}
        ${props.big ? "big" : "small"}
        `}
  />
);

export default Loader;
