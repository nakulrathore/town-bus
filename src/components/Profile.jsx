import React from "react";

//--assets
import user_placeholder from "../assets/images/user_placeholder.png";
import coin from "../assets/images/coin.svg";
import Loader from "../components/miscs/loader";
const Profile = props => {
  let profile = props.profile;
  let profileData = profile.data;
  return (
    <React.Fragment>
      {profile.isFetching ? (
        <div className="profile-loader">
          <Loader dark big />
        </div>
      ) : profile.error ? (
        <div className="profile-error">Something went wrong, please try again.</div>
      ) : (
        <section className="profile-container">
          <div className="balance">
            {profileData.balance}
            <img src={coin} alt="coin" />
          </div>
          <img className="user-placeholder" src={user_placeholder} alt="user placeholder" />
          <h3 className="name">{profileData.name}</h3>
          <span>+91 - {profileData.mobile_number}</span>
          <i className="address">{profileData.home}</i>
        </section>
      )}
    </React.Fragment>
  );
};

export default Profile;
