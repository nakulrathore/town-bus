import React, { Component } from "react";
import update from "immutability-helper";
//--
import RequestHandler from "./../lib/requestHandler";
//--
import Loader from "../components/miscs/loader";
import {yearRange, monthRange} from '../lib/_utils'

class Invoice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedYear: null,
      selectedMonth: null,
      pdfFileUrl: ""
    };
    // none of npm month picker was lighweight,
    // and safari dosnt allow native date picker

  }

  handleDateChange(type, value) {
    let newState = update(this.state, {
      $merge: {
        [type]: value
      }
    });
    this.setState(newState);
  }

  fetchInvoice = () => {
    let props = this.props;
    let state = this.state;
    let config = {
      requiredToken: true
    };
    let payload = { month: `${state.selectedMonth}/${state.selectedYear}` };
    props.responseHandler("invoice", "fetching");
    RequestHandler.request("GETFILE", "/get-invoice-of-user", payload, config)
      .then(res => {
        props.responseHandler("invoice", "success", "fetched");
        var newBlob = new Blob([res], { type: "application/pdf;base64" });
        let blobUrl = window.URL.createObjectURL(newBlob);
        this.handleDateChange("pdfFileUrl", blobUrl);
        window.open(blobUrl);
      })
      .catch(error => props.responseHandler("invoice", "failed", error));
  };

  downloadFile() {
    let state = this.state;
    var link = document.createElement("a");
    link.href = state.pdfFileUrl;
    link.download = `Invoice-${state.selectedMonth}-${state.selectedYear}.pdf`;
    link.click();
    setTimeout(function() {
      // for motzilla
      window.URL.revokeObjectURL(state.pdfFileUrl);
    }, 100);
  }

  render() {
    let state = this.state;
    let invoice = this.props.invoice;
    return (
      <section className="invoice-component">
        <h2>DOWNLOAD INVOICE</h2>
        {state.pdfFileUrl === "" ? (
          <section className="date-picker">
            <span className="info">Select a Year </span>
            <div className="year-range">
              {yearRange.map((year, index) => {
                return (
                  <div
                    onClick={this.handleDateChange.bind(this, "selectedYear", year)}
                    className={`year ${state.selectedYear === year ? "selected" : ""}`}
                    key={index}
                  >
                    {year}
                  </div>
                );
              })}
            </div>
            {state.selectedYear ? (
              <React.Fragment>
                <span className="info month">Select a month </span>
                <div className="month-range">
                  {monthRange.map((month, index) => {
                    return (
                      <div
                        onClick={this.handleDateChange.bind(this, "selectedMonth", index + 1)}
                        className={`month ${state.selectedMonth === index + 1 ? "selected" : ""}`}
                        key={index}
                      >
                        {month}
                      </div>
                    );
                  })}
                </div>
              </React.Fragment>
            ) : null}

            {state.selectedMonth && state.selectedYear ? (
              <button disabled={invoice.isFetching} onClick={this.fetchInvoice} className="invoice-button">
                {invoice.isFetching ? <Loader isLight /> : "Get Invoice"}
              </button>
            ) : null}
          </section>
        ) : (
          <React.Fragment>
            <h5 className="pdf-generated">
              pdf generated for {state.selectedMonth}/{state.selectedYear}
            </h5>
            <div className="view-pdf">
              <button className="download" onClick={this.downloadFile.bind(this)}>
                View Invoice
              </button>
            </div>
            <span onClick={this.handleDateChange.bind(this, "pdfFileUrl", "")} className="download-another">
              click here download another
            </span>
          </React.Fragment>
        )}
      </section>
    );
  }
}

export default Invoice;
