import React from "react";
import update from "immutability-helper";
import RequestHandler from "./../lib/requestHandler";
import { Redirect } from "react-router-dom";

import { getActionResult, isTokenAvailable } from "../lib/_utils";

// import "../../assets/styles/login.scss";
import busIcon from "./../assets/images/bus-icon.svg";
import Loader from "./miscs/loader";
class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      mobileNumber: "",
      isSubmitAllowd: true,
      otpResponce: {
        isFetching: false,
        data: {},
        error: null
      },
      loginResponse: {
        isFetching: false,
        data: {},
        error: null
      }
    };
  }

  validateValues = (type, value) => {
    if (type === "mobileNumber" && value.length !== 10) {
      return false;
    } else if (type === "otp" && value.length < 2) {
      return false;
    }
    return true;
  };

  formChangeHandler = inputType => event => {
    let eventValue = event.target.value;
    let newState = update(this.state, {
      $merge: {
        [inputType]: eventValue,
        isSubmitAllowd: this.validateValues(inputType, eventValue)
      }
    });
    this.setState(newState);
  };

  responseHandler = (responceType, action, data = null) => {
    let actionResult = getActionResult(action, data);

    let newState = update(this.state, {
      [responceType]: {
        $merge: {
          ...actionResult
        }
      }
    });
    this.setState(newState);
  };
  submitForm = () => {
    let {
      mobileNumber,
      otpResponce: {
        data: { otp = null }
      }
    } = this.state;
    let payload = { mobile_number: mobileNumber };
    if (!otp) {
      this.responseHandler("otpResponce", "fetching");
      RequestHandler.request("GET", "/get-otp-of-user", payload)
        .then(res => this.responseHandler("otpResponce", "success", res))
        .catch(error => this.responseHandler("otpResponce", "failed", error));
    } else {
      payload.otp = otp;
      this.responseHandler("loginResponse", "fetching");
      RequestHandler.request("GET", "/confirm-otp-of-user", payload)
        .then(res => {
          window.localStorage.setItem("access_token", res.token);
          this.responseHandler("loginResponse", "success", res);
        })
        .catch(error => {
          this.responseHandler("loginResponse", "failed", error);
        });
    }
  };

  render() {
    if (isTokenAvailable()) {
      return <Redirect to="/home/profile" />;
    }

    let {
      isSubmitAllowd,
      otpResponce: {
        isFetching: isOtpFetching,
        data: { otp = null },
        error: otpError
      },
      loginResponse: { error: loginError, isFetching: isLoginFetching }
    } = this.state;
    return (
      <section className="login">
        <img className="logo" src={busIcon} alt="logo" />
        <div className="form">
          <h3 className="heading">LOGIN</h3>

          <input
            type="tel"
            name="phone"
            onChange={this.formChangeHandler("mobileNumber")}
            placeholder="Mobile Number"
            required
            disabled={otp}
          />
          {otp ? <input value={otp} disabled={true} /> : null}
          {otpError || loginError ? <div className="erorr-toast">Error, try again.</div> : null}
          <button disabled={!isSubmitAllowd} type="submit" onClick={this.submitForm}>
            {isOtpFetching || isLoginFetching ? <Loader isLight /> : otp ? "Continue" : "Request OTP"}
          </button>
        </div>
      </section>
    );
  }
}

export default Login;
